function createMatrix(n) { //генерирование случайной матрицы размера
    if (n % 2 === 0 || n === 1) throw new Error('Необходимо передать нечётное число >= 3');

    const arr = new Array(n);
    for (let i = 0; i < n; i++) {
        arr[i] = new Array(n);
        for (let j = 0; j < n; j++) {
            arr[i][j] =  parseInt(Math.random(99) * 100);
        };
    };

    return arr;
};

function printElem(matrix) { //получение элементов матрицы в нужном порядке
    const n = matrix[0].length, //размерность матрицы
          Xc = (n - 1) / 2, //координаты центра (Xc = Yc)
          count = Xc; //количество квадратов
    let success = `${matrix[Xc][Xc]}`; //сюда будем приплюсовывать нужные элементры матрицы

    for (let i = 1; i < count + 1; i++) {
        const length = 2 * i + 1, //длина стороны квадрата
              countElem = length * 4 - 4; //кол-во элементов квадрата

        for (let j = 1, sum = 0; sum < countElem; sum++) {
            sum < countElem / 4 ? success += ` ${matrix[Xc - i + j][Xc - i]}` :
            sum < 2 *countElem / 4 ? success += ` ${matrix[Xc + i][Xc - i + j]}` :
            sum < 3 *countElem / 4 ? success += ` ${matrix[Xc + i - j][Xc + i]}` :
            success += ` ${matrix[Xc - i][Xc + i - j]}`;

            j === countElem / 4 ? j = 1 : j++;
        }
    }

    return success;      
}

const matrix = createMatrix(5);
console.log(matrix);
console.log('\n');
console.log(printElem(matrix));